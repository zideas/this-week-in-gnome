# This Week in GNOME

This repository contains the source code of [thisweek.gnome.org](https://thisweek.gnome.org) and contains the [Hebbot](https://github.com/haecker-felix/hebbot) configuration.
You can find more information about "This Week in GNOME" (short _TWIG_) in my [blog post](https://blogs.gnome.org/haeckerfelix/2021/07/16/introducing-this-week-in-gnome/).

To sum it up
1. Work on something GNOME related.
2. You have finished your work, and think other people should learn about it.
3. Join the [#thisweek:gnome.org Matrix room](https://matrix.to/#/#thisweek:gnome.org).
4. Ping _"TWIG-Bot"_ and write a short message in which you share what you've been working on.
5. The bot picks up your message and saves it.
6. Every Friday we publish a new issue on [thisweek.gnome.org](https://thisweek.gnome.org) with all the collected news.

## FAQ
1. How can I customize my name that will be used later for the blog post?

 The bot uses your Matrix account name. If you only want to change the name for TWIG, you can use the `/myroomnick <Name>` command in the #thisweek:gnome.org room to change it.

2. I want to change how my project appears in the blog posts. / How can I add my project?

 All project definitions are listed in the hebbot [`config.toml`](https://gitlab.gnome.org/World/twig/-/blob/main/hebbot/config.toml) file. If you want to edit the information for your project, please open a merge request!

3. Do I need Matrix to share news?

 Yes - the bot uses several matrix exclusive features (such as message reactions, or images / videos) that are not compatible with IRC (or would not work with an IRC bridge).

4. Are there rules for which news are accepted and which are not?

 We try to keep the whole concept as simple as possible, and therefore without a fixed set of rules. Ultimately, it is always a case-by-case decision whether we accept a news item or not. But the following rules of thumb can give you a rough feeling if we will include your news in the next blog post - or not:
 - It must be news about GNOME or a relevant technology (e.g. Flatpak).
 - It must be about significant changes, such as feature addition or a breaking change (e.g. updates that only contain updated translations are not considered newsworthy enough).
 - It should not promote unrecognized modifications (e.g. third party GTK themes).
 
 If in doubt, please don't hesitate to tell us your news! We try to collect as many news as possible!

If you have any further questions, you can contact us in [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org).

## Contribute
1. `git clone --recurse-submodules git@gitlab.gnome.org:World/twig.git`
2. Make sure to have `hugo` installed (`sudo apt install hugo` / `sudo dnf install hugo`)
3. Preview the page by running `hugo server -D` and opening http://localhost:1313

Useful information
- To add a new issue, you can use the 'new_post.sh' script
- The actual blog posts are located in `content/posts/`
- The TWIG page uses the Hugo [`Tale`](https://github.com/EmielH/tale-hugo) theme as base. 
- Theme / CSS customizations are in `static/custom.css` and `layouts`

cd /data

echo "Updating config.toml ..."
wget -nv -O config.toml https://gitlab.gnome.org/World/twig/-/raw/main/hebbot/config.toml

echo "Updating report_template.md..."
wget -nv -O report_template.md https://gitlab.gnome.org/World/twig/-/raw/main/hebbot/report_template.md

echo "Updating section_template.md..."
wget -nv -O section_template.md https://gitlab.gnome.org/World/twig/-/raw/main/hebbot/section_template.md

echo "Updating project_template.md..."
wget -nv -O project_template.md https://gitlab.gnome.org/World/twig/-/raw/main/hebbot/project_template.md

echo "Updating update_config.sh ..."
wget -nv -O update_config.sh https://gitlab.gnome.org/World/twig/-/raw/main/hebbot/update_config.sh

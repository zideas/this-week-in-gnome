---
title: "#64 Everything Green Again!"
author: Felix
date: 2022-10-07
tags: ["vala", "pika-backup", "flare", "secrets", "eyedropper", "gradience"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 30 to October 07.<!--more-->

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> GNOME's OpenQA testing initiative has seen some progress, the existing tests are all passing again, and the tests have migrated to a new [openqa-tests.git](https://gitlab.gnome.org/GNOME/openqa-tests) repo to enable much faster cycle times when developing tests. For more details, see: https://discourse.gnome.org/t/openqa-gnome-2022-edition/11427
> ![](qXyNhVsaOyQNPCOoTiotLQiC.png)

# Core Apps and Libraries

### Vala [↗](https://gitlab.gnome.org/GNOME/vala)

An object-oriented programming language with a self-hosting compiler that generates C code and uses the GObject system.

[lwildberg](https://matrix.to/#/@lw64:gnome.org) says

> Since GNOME Builder 43 was released, it does not bundle the Vala Language Server anymore. To continue getting diagnostics, completion and more code intelligence features, you need to use the language server from [Vala's Flatpak Sdk](https://github.com/flathub/org.freedesktop.Sdk.Extension.vala). To enable it in your project, add these lines to your Flatpak manifest:
> 
> ```
> {
>     "id" : "org.example.MyApp",
>     "runtime" : "org.gnome.Platform",
>     "runtime-version" : "43",
>     "sdk" : "org.gnome.Sdk",
>     "sdk-extensions" : ["org.freedesktop.Sdk.Extension.vala"],
>     "build-options" : {
>         "append-path" : "/usr/lib/sdk/vala/bin",
>         "prepend-ld-library-path" : "/usr/lib/sdk/vala/lib"
>     },
> ...
> ```
> 
> For everyone else not developing using Flatpak, you need to install the language server on your system. Of course this can also be done through Toolbx or similar technologies.

# Circle Apps and Libraries

### Secrets [↗](https://gitlab.gnome.org/World/secrets)

A password manager which makes use of the KeePass v.4 format.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> Secrets 7.0 was just released! Numerous bug fixes and improvements were added
> 
> * Basic check for file conflicts
> * Password history support
> * Support for trash bin
> * Redesigned custom entry attributes
> ![](468816ea7b133b3fbec1717672bbef50d6da16f2.png)

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> Pika Backup gained support for excluding directories that contain a `CACHEDIR.TAG`. [Cached Directory Taggings](https://bford.info/cachedir/) is a general specification that allows apps to exclude folders from backups. It is for example used by the Rust language to mark its `target` directories.
> 
> In further exclusion news: There is now a rudimentary dialog to exclude folders or files via shell type patterns and regular expressions.
> ![](b140442d655a907e321d42fe6c4f4b54ca1c3557.png)

# Third Party Projects

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> Announcing [Playhouse](https://github.com/sonnyp/Playhouse) 1.0! 
> 
> Playhouse is a playground for HTML/CSS/JavaScript which makes it easy to prototype, teach, design, learn and build Web things.
> 
> Features:
> 
> * HTML/CSS/JavaScript editors
> * live Web preview
> * auto-save
> * developer tools
> * dark/light mode support
> 
> Powered by GTK4, WebKitGTK, GtkSourceView and GJS
> ![](riYjsdayWXNKqOlgbanywgJv.png)

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> [Tagger](https://beta.flathub.org/apps/details/org.nickvision.tagger) V2022.10.0 is here! We put a lot of work into this release: re-working the tag editing experience, and thus more intuitive and easier to use. We've also added a new preference for managing MusicBrainz metadata and fixed many bugs as well.
> Here's a full changelog:
> * Tagger now requires clicking the 'Apply' button to save changes to selected tags after inserting/removing album art, converting filenames to tags, or downloading metadata from MusicBrainz. Changing a file selection without clicking 'Apply' will preserve the changes to the tags, however, if the music folder is changed/reloaded or if the application is closed without clicking 'Apply', the changes will be lost. Deleting Tags continues to be a permanent action that will take affect as soon as the action is confirmed from the message box
> * Added the 'Overwrite Tag With MusicBrainz' option to preferences
> * Added the ability to click on the album art in the tag properties pane to insert album art
> * FIxed an issue where Tagger wouldn't handle UTF-8 characters correctly
> * Fixed an issue where applying a filename change was not updating the must files list
> ![](IiviEIfWMOfNTeiextmOohbz.png)

[Romain](https://matrix.to/#/@romainvigier:matrix.org) announces

> I released the first version of [Zap](https://gitlab.com/rmnvgr/zap), a new soundboard application. [Download it from Flathub](https://flathub.org/apps/details/fr.romainvigier.zap) and make your livestreams and videocasts more entertaining!
> ![](sdzZcZDvDMpZiRVUBYUNtGpY.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[Daudix UFO](https://matrix.to/#/@daudix_ufo:matrix.org) says

> This week, Gradience gained some UI polish, usability and under-hood improvements, some of them:
> 
> * Preset Manager now opens immediately
> * "Log Out" message after applying theme
> * Improved UI of Preset Manager
>     - Now presets can be starred
>     - Added preset repo switcher with which you can display only presets from specific repo
> * All contributors now listed in "About" window, **let us know if you are not listed**
> * Text now follows GNOME Writing Guidelines
> * Fixed flatpak theming
> * Added repo template for user preset sharing
> 
> This changes will be available in version 0.3.1 that will be available on [Flathub](https://beta.flathub.org/apps/details/com.github.GradienceTeam.Gradience) very soon.
> ![](PSDAESBajWtFJcZasGSIWSlS.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

A unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> [Flare](https://flathub.org/apps/details/de.schmidhuberj.Flare) 0.5.0 was released. Besides some important bug-fixes, Flare gained the ability to search for contacts, show notifications and has seen many usability- and user interface improvements. Furthermore, the new message dialogs and about window of libadwaita are now used.
> ![](cuMnXLiXAbnjJxinzphEiGYR.png)
> {{< video src="wBQYephJpIkiNvgzktAGBUTn.mp4" >}}

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

A powerful color picker and formatter.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> I've implemented in [Eyedropper](https://flathub.org/apps/details/com.github.finefindus.eyedropper) the long awaited feature of a palette dialog, suggesting different colors schemes based on the currently picked color. It now also uses libadwaita 1.2 and the new AdwAboutWindow.
> ![](UotaPpbBaYwTbvGvWGJDzUPs.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


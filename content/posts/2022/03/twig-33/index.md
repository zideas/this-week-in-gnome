---
title: "#33 Fabulous Screenshots"
author: Felix
date: 2022-03-04
tags: []
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 25 to March 04.<!--more-->

# Miscellaneous

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> Chris 🌱️ and I launched the [Update App Screenshots Initiative](https://gitlab.gnome.org/GNOME/Initiatives/-/issues/34). Our short-term goal is to have up-to-date screenshots for all Core apps for the upcoming GNOME 42 release. So far, for 15 of 30 Core apps, merge requests are created or already merged.
> 
> If you are maintaining or contributing to an app, you can have a look at our [screenshot guidelines](https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/Update-App-Screenshots), and if needed update your screenshots accordingly.

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [flatpak-vscode](https://github.com/bilelmoussaoui/flatpak-vscode) 0.0.21 is out with support of 
> 
> * New Flatpak manifest selector
> * Watch for Flatpak manifests changes and modify state accordingly
> * Support JSON manifests with comments
> * Support `--require-version`
> * Better state management

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> I have released an updated version of "Audio Sharing". The interface has been adapted to the new Adwaita design, and some bugs that prevented streaming have been fixed.
> 
> In case you don't know it yet - with this small tool you can stream the audio playback from your computer to other devices in your local network.
> You can find a more detailed description on the [project homepage](https://gitlab.gnome.org/World/AudioSharing). It is available for download on [Flathub](https://flathub.org/apps/details/de.haeckerfelix.AudioSharing).
> ![](LglYdcTrysrNIdBOXlbBAmBj.png)

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> The first release of Workbench is out
> 
> Workbench is
> 
> * A sandbox to learn and experiment with GNOME technologies
> * A developer tool for testing and building with an instant feedback loop
> 
> https://flathub.org/apps/details/re.sonny.Workbench
> ![](JakhAxNRhhrQFEbzscEpLBLU.png)

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> The Getting Started tutorial for newcomers to GNOME application development is now complete. You can follow it to learn how to use GNOME Builder to write your own GNOME application; loading and saving content with asynchronous operations; changing the style of your application; adding menus; and saving preferences. The documentation is ready for GNOME 42 and libadwaita: https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) says

> The [Desktop-Cube extension](https://extensions.gnome.org/extension/4648/desktop-cube/) for GNOME Shell has been updated and brings [many new features](https://github.com/Schneegans/Desktop-Cube/blob/main/docs/changelog.md)! Most importantly, you can now freely rotate the cube by click-and-drag. This works in the overview, on the desktop, and on the panel. The latter is especially cool if you have a maximized window!
> 
> Other changes include:
> * Support for GNOME 42.
> * Proper touch-screen support.
> * Support for [online-translations](https://hosted.weblate.org/engage/desktop-cube/).
> 
> You can [watch a trailer on YouTube](https://www.youtube.com/watch?v=J7pdnkv7v1A)!
> ![](enRenYcytitNDBKAonuYoBpZ.jpg)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


---
title: "#38 Among Toasts And Tabs"
author: Felix
date: 2022-04-08
tags: ["libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 01 to April 08.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> A few changes to `AdwToast`: Kévin Commaille added [a way to set custom widgets as titles](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.Toast.custom-title.html), while Emmanuele Bassi added a convenience [`adw_toast_new_format()`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/ctor.Toast.new_format.html) constructor.

> AdwTabBar style has got updated. Which tab is selected should be much more obvious now, especially in dark variant or with only 2 tabs open
> ![](2df2d6898115a709e9fd8d91474950a217aadacb.png)
> ![](61be8c6bbafffcdec1c57339dd358c61796211ef.png)

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> I have added a [use-markup property](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.PreferencesRow.use-markup.html) to `AdwPreferencesRow`. Previously, classes like `AdwActionRow` always treated values for `title` and `subtitle` as Pango markup. The new property can be used to disable this behavior. This is especially useful if the values are obtained from external data/input. You might want to check your code if you have missed this detail.
> 
> For `AdwComboRow` the default value for `use-markup` will be `FALSE`. This is because the default factories do not expect Pango markup. Therefore the `use-subtitle` property is incompatible with the previous behavior of the subtitle interpreting the subtitle as Pango markup.

# Third Party Projects

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Niels De Graef [ported uhttpmock to Meson](https://gitlab.com/uhttpmock/uhttpmock/-/merge_requests/13/). uhttpmock is a library for making it easier to test clients of HTTP/REST APIs offline.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


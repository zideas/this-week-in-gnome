---
title: "#61 Overview Tabs"
author: Felix
date: 2022-09-16
tags: ["workbench", "gdm-settings", "bottles", "cawbird", "apostrophe", "libadwaita", "pods"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 09 to September 16.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> I've released libadwaita 1.2.0: https://blogs.gnome.org/alexm/2022/09/15/libadwaita-1-2/

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> and immediately following 1.2.0, [`AdwTabOverview`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.TabOverview.html) and [`AdwTabButton`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.TabButton.html) have landed
> {{< video src="ddd336b77b7fae394d762e08ecdee11b4280b0a1.mp4" >}}

# Circle Apps and Libraries

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) reports

> I've finished porting Apostrophe to GTK4. There are still things to iron out but a beta release should be around the corner. The whole UI has been improved, tons of bugs fixed and there are some new features like selection stats. Some things didn't make it though: there's no spellchecking support at the moment and the preview popover has been removed due to poor discoverability, although I have plans to have an even better replacement.
> ![](490cb62aff505551d3d845377efcd3ccf1bd9a71.png)

# Third Party Projects

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Version 0.4.0 of [Flare](https://gitlab.com/Schmiddiii/flare), an unofficial Signal client, was released. Besides some smaller improvements, this includes support for persistent message storage. This greatly increases the usability of the application compared to previous versions.
> ![](sxFhNxojYVfHqpKunPhHEPXX.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> [Workbench](https://github.com/sonnyp/Workbench) gained support for inline CSS errors emitted directly by the [GtkCssProvider](https://docs.gtk.org/gtk4/class.CssProvider.html). Now that both Blueprint and CSS support inline errors, you can design user interfaces with confidence without looking at the console.
>
> In addition, sluggishness has been solved on large Blueprint documents.
> ![](CaafrfcnxkWgZEXzXnYOghhY.png)
> ![](aFJZPtDgitMJhKxTrVSwVOeB.png)

### Pods [↗](https://github.com/marhkb/pods)

A podman desktop application

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) reports

> The last update of [Pods](https://github.com/marhkb/pods) in TWIG was on April 22. Since then, a lot has happened. Several hours and gallons of coffee have gone into lots of new features. The most important among these are
>
> * an application icon created by [allaeddineomc](https://github.com/allaeddineomc)
> * the provision of basic functions for pods (overview, detail view, start, stop, delete, ...)
> * the management of connections to different Podman instances
> * bulk actions like starting or deleting multiple containers at the same time.
> * the ability to build images from Dockerfiles
> * a process viewer for containers and pods
> * views for raw inspection data of images/containers/pods
> * insights about the health status of a container
> * several other small features and improvements
>
> Last but not least, I would like to mention that Pods is now available on Flathub Beta.
> ![](TwKvWpmibmYgKuxiWmhTGnlO.png)
> ![](pUteuKUFjYHtLKCdVdYcVhun.png)
> ![](OJxnAFYWsCsrMntIIvTWkUkN.png)
> ![](LEtLUpGYMlHyTQeasPlDNuIe.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) has received some updates since the first beta for v1.0 was released. The latest beta version is 1.0-beta.4.
>
> *  A real (working) AppImage is available now. You can download it from [here](https://github.com/realmazharhussain/gdm-settings/releases/download/v1.0-beta.4/Login_Manager_Settings.AppImage).
> * Screenshots have been updated
> * Added support for incomplete shell themes
> * If value of `--verbosity` option is invalid, the app refuses to launch now instead of assuming the maximum value.
> * Fixed: Flatpak version of the app could not change logo
> * Fixed: When extracting default shell theme, it would be saved directly to `/usr/local/share` directory instead of `/usr/local/share/themes`
> * Fixed: Release info for version 1.0-beta.2 was not being shown in GNOME Software
> ![](sVEmARAlbAfeQIIjCbRVVLVh.png)

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) says

> In this week, the GTK4 rewrite of Cawbird recieved the ability to hide replies from your timelines. If a thread is present in your timeline, it will also be displayed as an thread.
> Additionally, the initial part of the preferences window was added, which includes apart from a few appearance settings the graphical interface to add and remove accounts.
> ![](kjxalpSMgWPwWjMfDsnHDvdj.png)
> ![](hgtiKvhbUUXKByescWOyeZAX.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Bottles was recently [ported](https://github.com/bottlesdevs/Bottles/commit/94bc8ffb73fa9bbd979e11cef10f884e716ef790) to Blueprint! [Blueprint](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/) is a new markup language to create GTK interfaces. It is more  efficient for development as the syntax is less verbose and more visually pleasing in contrast to XML. Thanks to its efficiency, Bottles's codebase was reduced by 800 lines!

# Miscellaneous

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> [It was announced](https://blogs.gnome.org/aday/2022/09/15/gnome-info-collect-closing-soon/) that there have been an amazing 2,200 responses to gnome-info-collect. Data collection is scheduled to end on Monday 19 September.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
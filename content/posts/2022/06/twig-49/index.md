---
title: "#49 New Views"
author: Felix
date: 2022-06-24
tags: ["nautilus", "pika-backup", "gnome-calendar"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 17 to June 24.<!--more-->

# Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a simple and integrated way of managing your files and browsing your file system.

[antoniof](https://matrix.to/#/@antoniof:gnome.org) reports

> The fresh new Files list view mode has arrived in Nightly, with [many bugfixes and new features](https://gitlab.gnome.org/GNOME/nautilus/-/commit/6708861ed174e2b2423df0500df9987cdaf2adc0).
> ![](c15b648b97416c4dcbca1c1405f87d1e831712a0.png)

[Ignacy Kuchciński](https://matrix.to/#/@ignapk-matrix:matrix.org) announces

> Hello there! My name is Ignacy Kuchciński and I've been selected for the GSoC'22 program to improve the discoverability of the "New Document" feature in Nautilus, the file manager for GNOME, under the guidance of my mentor Antonio Fernandes.
> 
> You can follow my progress with the project by checking out my blog [here](https://ignapk.blogspot.com/). You'll find there a short [initial post](https://ignapk.blogspot.com/2022/05/gsoc-2022-introduction.html) introducing me, as well as first [update post](https://ignapk.blogspot.com/2022/06/gsoc-2022-first-update-planning.html) that talks about our plans and milestones for the project.

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) says

> Calendar [now has a pinch gesture for its week view](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/239). It works on touchscrens and touchpad as well as by scrolling while holding the Ctrl key. It will help the application work nicely on small touchscreens like GNOME mobiles.
> {{< video src="cf2f4dbd64e8f55026e32f52ee25e096af14e7ce.webm" >}}

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) says

> Calendar's events [received a fresh coat of paint](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/237). The new style better matches the latest designs and supports the dark style.
> ![](83d78c05171de424ac22d431ea0fae7b9e9020c4.png)
> ![](c153747cbfbd00b582a3a70701f89c233d31d22a.png)
> ![](15b31edc835cae92bcb82bceea36af76a5a81059.png)
> ![](3a182024062ebefcd04b077502ac3892a89149a1.png)

# Circle Apps and Libraries

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> [Pika Backup](https://apps.gnome.org/app/org.gnome.World.PikaBackup/) 0.4.1 has been released today. It mostly addresses one external problem with scheduled backups and updates several translations.
> 
> The new version contains a workaround that fixes scheduled backups not working on some systems when using Flatpak. The systems affected are or were missing a [fix for Flatpak’s auto start functionality](https://github.com/flatpak/xdg-desktop-portal/releases/tag/1.14.4).
> 
> The new version also comes with BorgBackup 1.2.1 on Flathub which brings several minor fixes.

# Third Party Projects

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v0.6](https://github.com/realmazharhussain/gdm-settings/releases/tag/v0.6) has been released with some very important bug fixes.
> 
> ### Bug fixes
> 
> * A lot of Fedora (and other SELinux enabled distros) users were experiencing their Login Manager breaking after using this app. This has been fixed.
> * On Ubuntu, some settings (Shell Theme, Background, Top Bar Tweaks) were not getting applied. This has also been fixed.
> * Some other minor bugs were fixed.
> 
> ### New features
> 
> * Command-line options for verbosity and printing application version have been added.
> * The application got a new GitHub Pages [website](https://realmazharhussain.github.io/gdm-settings) with a nice [Show in App Store](appstream://io.github.realmazharhussain.GdmSettings) link.
> 
> ### Translations
> 
> * 4 new languages
> * 5 languages updated
> * Switched to [Weblate](https://hosted.weblate.org/engage/gdm-settings) for translations.
> 
> Click [here](https://github.com/realmazharhussain/gdm-settings/releases/tag/v0.6) for full changelog.
> ![](PfAurdXrmsnzzrPisXGxsXWx.png)

# GNOME Shell Extensions

[Advendra Deswanta](https://matrix.to/#/@adeswanta:matrix.org) announces

> [Lock Screen Message](https://gitlab.com/AdvendraDeswanta/lock-screen-message/) is released with GNOME 42 & libadwaita support and longer text feature! (max 480 chars width). So you can add your message with a longer explanations.
> ![](DBakrtROjEMckzZiXCDGnzEI.png)

[Advendra Deswanta](https://matrix.to/#/@adeswanta:matrix.org) announces

> [Shell Configurator](https://gitlab.com/adeswantaTechs/shell-configurator) v5 has been released after more than a year since the latest version (v4) was released and suspended the development.
> 
> ### This extension comes with **BIG** changes, including:
> 
> * Added GNOME 41 & 42 (with libadwaita) Support
> * Rewritten and redesigned preferences look
> * New extension preset and configuration search feature
> * Added suggested extension section
> * Added more configurations!
> * Support more than 10 supported languages
> * Configuration module system
> * Bug fixes
> 
> and more changes on [CHANGELOG.md](https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/CHANGELOG.md).
> 
> You can install this extension on [extensions.gnome.org](https://extensions.gnome.org/extension/4254/shell-configurator/)
> 
> You can also contribute this extension by following these rules on [CONTRIBUTING.md](https://gitlab.com/adeswantaTechs/shell-configurator/-/blob/master/CONTRIBUTING.md). All contributors are welcome.
> ![](ytvpBHWEZrWHkwiTuGSYkvlz.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

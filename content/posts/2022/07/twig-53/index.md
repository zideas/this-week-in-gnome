---
title: "#53 GUADEC 2022"
author: Felix
date: 2022-07-22
tags: ["health", "workbench", "commit"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 15 to July 22.<!--more-->

# GNOME Foundation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> GUADEC 2022, the GNOME conference, is currently under way in Guadalajara, Mexico. On Thursday, during the Annual General Meeting of the GNOME Foundation, the famed Pants Award given by the Foundation to a member of the GNOME community in recognition of their work, was awarded to Sophie Herold for her sterling work on the GNOME Circle and Apps of GNOME initiatives.
>
> [![](guadec.jpg)](https://events.gnome.org/event/77/)

# GNOME Releases

[Jordan Petridis](https://matrix.to/#/@alatiera:matrix.org) announces

> The first alpha of GNOME 43 was released! The Highlights include:
> * The new Libadwaita widgets!
>   * AdwAboutWindow
>   * AdwEntryRow and AdwPasswordEntryRow
>   * AdwMessageDialog
> * Builder and Files are now ported to GTK 4
> * Epiphany now has HTTP/2 support, as well as Web Extensions and Web apps improvments
> 
> Read the announcement and the complete changelog [here](https://discourse.gnome.org/t/gnome-43-alpha-released/10491)

# Core Apps and Libraries

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Parental Controls is now ported to GTK4 and libadwaita
> ![](49f0989caa6cea2c1033ccf6cee0c0b20d8ba28d.png)
> ![](3ccca22bf17fd9133fa7d372298bf382c4f727c4.png)

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:gnome.org) announces

> Health version 0.94.0 has been released, including many bugfixes and more reliable notifications.

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> A new release of [Commit message editor](https://apps.gnome.org/app/re.sonny.Commit/) is out. It adds a theme switcher, better dark mode support, improved keyboard support and a fix for auto-capitalization when amending.
> ![](TaUhQiSrRUmUxCjAAnCTeIAY.png)

# Third Party Projects

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> A new release of [Workbench](https://github.com/sonnyp/Workbench/) is out with crash fixes and underlining of errors in Blueprint code using the [Language Server Protocol](https://microsoft.github.io/language-server-protocol/).
> 
> Inline display of error messages is ready, but will have to wait for GNOME platform 43.
> ![](TOBIvCLWGfEooKriaTBcQGhd.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

---
title: "#32 Security Issues"
author: Felix
date: 2022-02-25
tags: ["webkitgtk", "portfolio", "phosh", "gnome-builder"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 18 to February 25.<!--more-->

⠀

---

_A message from the TWIG team_

This issue takes place at a time when a war has broken out in Europe. In the current time we receive horrific news from Ukraine. 

We stand in solidarity with all people affected by this terrible war.


🇺🇦

---

# Core Apps and Libraries

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[adrian](https://matrix.to/#/@aperez:igalia.com) says

> WebKitGTK has received two releases recently, [2.34.5](https://webkitgtk.org/2022/02/09/webkitgtk2.34.5-released.html) and [2.34.6](https://webkitgtk.org/2022/02/17/webkitgtk2.34.6-released.html). Both contain a number of fixes for security issues—one of them for remote code execution, known to be exploited in the wild. We have also solved touch input handling, made accessibility work again when the Bubblewrap sandbox is in use, and improved video codec selection with GStreamer 1.20.

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[gwagner](https://matrix.to/#/@gwagner:gnome.org) announces

> GNOME Builder contains now templates for Adwaita, Gtk4 and Gtk3 for C, Rust, Python, Gjs and Vala. In order to make the templates easier to find we increased the initial size of the window. Besides of that we fixed several crashes related to terminal spawning in Builder.

# Third Party Projects

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) reports

> The simple math logic game [Multiplication Puzzle](https://flathub.org/apps/details/net.launchpad.gmult) got a new port to gtk4 and libadwaita, and is now available on flathub!
> ![](6b0c53e40197bcceeec7a0ee4a01660679b4f4a7.png)

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> Announcing [Login Manager Settings](https://github.com/realmazharhussain/gdm-settings)  (gdm-settings): a 'Settings' app for GNOME's Login Manager. It can change a lot of settings including shell theme and wallpaper of the login screen. It does not have any pre-built packages yet (except for an AppImage without any dependencies and AUR packages).
> ![](IwRSZZiLLeCiwGDEfWEATYEJ.png)

### Portfolio [↗](https://github.com/tchx84/Portfolio)

A minimalist file manager for those who want to use Linux mobile devices.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> After a few months of slow but steady progress, Portfolio 0.9.13 is out 📱🤓! This new release comes with the ability to fully manage external devices, improved feedback and responsiveness when copying big files to slow devices and many bugs fixes.
> 
> https://blogs.gnome.org/tchx84/2022/02/20/portfolio-0-9-13/
> ![](wbGceEklVWAZtrgIIzuVlGRV.gif)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> I've tagged phosh 0.16.0 with nice fading labels in the overview (by Adrien), more style updates by Sam, a keypad that can shuffle the buttons around and a bunch of fixes.
> {{< video src="gDZsIFsLyhCqhPhUbwSxEQyv.mp4" >}}

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> The "Getting Started" tutorial for newcomer app developers on the [GNOME Developer Documentation website](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) now has code examples in Python alongside the C ones.

# GNOME Shell Extensions

[Romain](https://matrix.to/#/@romainvigier:matrix.org) says

> I ported my [Night Theme Switcher extension](https://nightthemeswitcher.romainvigier.fr/) to GNOME 42. It brings scheduling abilities to the integrated dark mode and adds a way to set the built-in light and dark desktop background images.
> ![](tyvBLQkBslQmBbhiYJQKDEQu.png)

[Alex Saveau](https://matrix.to/#/@supercilex:matrix.org) says

> Announcing [Gnome Clipboard History](https://github.com/SUPERCILEX/gnome-clipboard-history) (GCH): a rewrite of Clipboard Indicator with vastly improved performance, bug fixes, and new features. What's the big innovation? GCH uses a compacting log and linked list to store data, enabling minimal O(1) performance for almost all operations. If you want all the details, I wrote up a blog post about it [here](https://alexsaveau.dev/blog/projects/performance/gnome/gch/gnome-clipboard-history).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


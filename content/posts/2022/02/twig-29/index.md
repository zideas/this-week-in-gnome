---
title: "#29 New Year, New Calendar"
author: Felix
date: 2022-02-04
tags: ["libhandy", "gjs", "phosh", "gnome-shell", "glib", "gnome-calendar"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 28 to February 04.<!--more-->

# Core Apps and Libraries

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> I have ported most of GNOME Calendar to GTK4, and it will likely be ready in time for the GNOME 42 release.
> ![](2f3e507ec7286175444f431aa086e3506fab3fab.png)
> ![](c060757f73470f5de6159d3cb065178a1059f8bb.png)
> ![](35163643f50bfba34b2c4730947a39262b5a4e6a.png)
> ![](d82d0ea74c3421df1337ff50db9b82c4d6139f78.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> The screen recording part of the new screenshot UI has been merged for GNOME 42. Now there are only a few utility things left to merge, some design tweaks and bug fixes.

[TheOPtimal](https://matrix.to/#/@optimal:kde.org) says

> Rounded corners have been [removed in GNOME 42,](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2151). This should help with performance in the future.
> 
> For technical reasons, see [here](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2151#note_1374058)
> ![](78cc4506aeca99ddf01a66ee325af197be81aa11.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Christian Hergert has [landed support for property binding groups and signal groups](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2235) to GLib, which allow multiple bindings or signal connections to be connected/disconnected to a GObject at once

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) announces

> In GJS this week,
> * Evan Welsh switched on support for [`WeakRef`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakRef) and [`FinalizationRegistry`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry) in our runtime. Use these with care, but they may offer some solutions to problems with circular references.
> * Marco Trevisan made it possible to pass BigInt values to GObject-introspected functions with 64-bit parameters. This way, you can finally work with large numbers that cannot be accurately stored as a JS Number value and pass them correctly into C. For example, `GLib.Variant.new_int64(2n ** 62n)`
> * To go along with this, I added `GLib.MAXINT64_BIGINT`, `GLib.MININT64_BIGINT`, and `GLib.MAXUINT64_BIGINT` constants to the GLib module.
> * I fixed a bug that broke passing the `NONE` Gdk.Atom value into a function.
> 
> As the feature freeze approaches, look for some performance fixes and some exciting developments around modules in next week's edition!

# Third Party Projects

[nirbheek](https://matrix.to/#/@nirbheek:matrix.org) says

> GStreamer 1.20 has been released after almost 1½ years of work. Some of the highlights that might be interesting to GNOME developers are:
> 
> 1. Development in GitLab switched to a single git repository containing all the modules, and the development branch was switched from `master` to `main`.
> 2. GstPlay: new high-level playback library, replaces GstPlayer
> 3. Runtime compatibility support for libsoup2 and libsoup3 (libsoup3 support is experimental)
> 4. The new VA-API plugin implementation `va` was fleshed out with more decoders and new postproc elements
> 5. AV1 hardware decode support was added to the old VA-API plugin `vaapi`, the new VA-API plugin `va`, and Intel Media SDK `msdk`
> 6. Video decoder subframe support
> 7. Smart encoding (pass through) support for VP8, VP9, H.265 in encodebin and transcodebin
> 8. Audio support for the WebKit Port for Embedded (WPE) web page source element
> 9. Many WebRTC improvements, including video decoder automatic packet-loss, data corruption, and keyframe request handling
> 10. More software video conversion fast-paths
> 11. Linux Stateless CODEC support gained MPEG-2 and VP9
> 12. mp4 and Matroska muxers now support profile/level/resolution changes for H.264/H.265 input streams (i.e. codec data changing on the fly)
> 13. Lots of new plugins, features, performance improvements and bug fixes
> 
> For more details including improvements on Windows, Android, iOS, macOS, and Embedded Linux, see the [GStreamer 1.20 release notes](https://gstreamer.freedesktop.org/releases/1.20/).

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> Announcing [Workbench](https://github.com/sonnyp/Workbench) ! - An application to learn and prototype with GNOME development.
> 
> Features GTK/CSS live preview.
> Please note that this is still a work in progress but I'm gathering feedback before release.
> 
> See [here](https://github.com/sonnyp/Workbench#workbench) for instructions.
> 
> Powered by GJS, Vte, GTK4, libadwaita and GtkSourceView.
> 
> Thanks Tobias Bernard for the icon !
> ![](CHPEPprdvDKNHUFvgPQKaQlB.png)

[dabrain34](https://matrix.to/#/@dabrain34:matrix.org) reports

> GStPipelineStudio 0.2.0 is out !! 🎇 Hope you'll enjoy it !! Here is the release notes: https://gitlab.freedesktop.org/dabrain34/GstPipelineStudio/-/tags/0.2.0

[Martin Lund](https://matrix.to/#/@lundmar:matrix.org) reports

> I've just released lxi-tools v2.0.
> 
> lxi-tools is a collection of open source software tools for managing network attached LXI compatible test instruments such as modern oscilloscopes, power supplies, spectrum analyzers, etc.
> 
> Features include automatic discovery of test instruments, sending SCPI commands, grabbing screenshots from supported instruments, benchmarking SCPI message performance, and powerful scripting for test automation. Both a command-line tool and a GUI tool are available.
> 
> The v2.0 release marks a total rewrite of the lxi-gui application using GTK4 / libadwaita which renders it a very modern GUI application. The script feature makes use of gtksourceview which made it really easy to integrate a custom script editor with useful editing features. I think the combination of these technologies have helped make lxi-gui easier to use and look amazing despite the nature of the application - managing complex test instruments.
> 
> See https://lxi-tools.github.io for more details and how to get involved with the project.
> ![](dXCmPcvKNIBvyYsndKKmTLfA.png)
> ![](HAgJVIXCrNYxlvwkPNziDDoM.png)
> ![](DMGeqfoVLTeVdOiqQtXjeRla.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> Last week we released [phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.15.0 featuring
> 
> * Swipeable notification frames
> * VPN quicksetting, authentication and status icon
> * Support for arbitrary passwords
> 
> and more. Check the full [release
> notes](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.15.0) for more
> details and contributors.
> 
> Since then we merged more style updates by Sam Hewitt affecting the lock screen and OSD:
> ![](OYcxYvkhpMiqVlTdxfzkoBXz.png)

# Documentation

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) reports

> Libsecret documentation has been ported to gi-docgen, it can now be found at https://gnome.pages.gitlab.gnome.org/libsecret/.

### Libhandy [↗](https://gitlab.gnome.org/GNOME/libhandy)

Building blocks for modern GNOME apps using GTK3.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> Maximiliano has ported libhandy [docs](https://gnome.pages.gitlab.gnome.org/libhandy/doc/main/) to [gi-docgen](https://gitlab.gnome.org/GNOME/gi-docgen/) and significantly cleaned them up
> ![](ee078c8edd976e45cc9c8584caf8e69b1ea21106.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) says

> The just-for-fun GNOME Shell extension [Burn-My-Windows](https://extensions.gnome.org/extension/4679/burn-my-windows/) added support for window-open effects! Furthermore, it now includes a Broken-Glass effect, added preview buttons to the effect configuration pages, and brings [translation support](https://hosted.weblate.org/engage/burn-my-windows/). Watch the trailer: https://youtu.be/L2aaNF_rPHo
> ![](XTtUuHdAchitUVcFjzrxAujA.gif)

[Advendra Deswanta](https://matrix.to/#/@adeswanta:matrix.org) says

> I made GNOME Shell Extension called [Lock Screen Message](https://gitlab.com/AdvendraDeswanta/lock-screen-message). It's a simple extension that lets you add your message to the lock screen. It has same functionality as Android lock screen owner info and now it's available on [GNOME Shell Extensions](https://extensions.gnome.org/extension/4801/lock-screen-message/).
> ![](TsuUuTXfBbWtxHCBcdmgoHvD.png)

# Miscellaneous

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) announces

> Some of us in Berlin sonnyp, Tobias Bernard,  verdre, robert.mader and zeenix  gathered for a hackaton last weekend, it was tons of fun and the opportunity to share, learn and make progress on
> * GNOME OS: testing, disk encryption, install guide for developers
> * GNOME Shell: hardware video encoding, fractional scaling, better multi-GPU support, debugging, centering new windows
> * Divers: new apps, zbus, homed support
> 
> More on those later, stay tuned 😌

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!


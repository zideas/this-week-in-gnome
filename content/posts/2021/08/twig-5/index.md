---
title: "#5 Freeze Frenzy"
author: Thib
date: 2021-08-13
tags: ["gnome-software", "fractal", "nautilus", "metronome", "gjs", "mutter", "libadwaita", "decoder", "connections", "metadata-cleaner"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 06 to August 13.<!--more-->

# Core Apps and Libraries

### Files [↗](https://gitlab.gnome.org/GNOME/nautilus)

Providing a  simple and integrated way of managing your files and browsing your file system.

[Allan Day](https://matrix.to/#/@aday:gnome.org) says

> in Files, Ondrej Holy [added support for creating encrypted archives](https://gitlab.gnome.org/GNOME/nautilus/-/merge_requests/682).
> ![](1e7852a441836b231c3334bc87fdaebb7b5a1718.png)


### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Workspace transitions are [now](https://gitlab.gnome.org/GNOME/mutter/merge_requests/850) more seamless, with windows on both workspaces appearing focusing instead of switching at the end of transition.
> ![](41927c4bd004228343495f4cbd55c71043b4062c.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> libadwaita now supports style classes [`.numeric`](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/217) for easily making a widget use tabular figures, and [`.pill`](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/222) for buttons, as seen in applications like Clocks.
> ![](a8940e23e87caa94aa4141399fd63efcabef28b4.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Allan Day](https://matrix.to/#/@aday:gnome.org) says

> Milan Crha's [rework of the respository preferences](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/812#note_1245491) was merged. Adrien Plazas added [his own improvements](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/900), as well as [improving the UI of the recently installed updates dialog](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/881).
> ![](ec57abdd32b3bb33c07d473bd65e7342d5fd8043.png)

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> Milan Crha [improved the layout of the OS upgrade banners](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/896).
> ![](b44fd1e8b2e7c53e015600f865a8a18a583c36bf.png)

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) announces

> In GJS, Evan Welsh added the [`TextEncoder`](https://developer.mozilla.org/en-US/docs/Web/API/TextEncoder) and [`TextDecoder`](https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder) global objects. Use these in your new code, instead of importing the old `imports.ByteArray` module.
> 
> To the GJS debugger Florian Müllner added an option, `set ignoreCaughtExceptions`, on by default, to not break on exceptions if they are already going to be caught elsewhere in the code.
> 
> More bug fixes in GJS: Florian fixed `Gtk.Widget.install_action()` and `GObject.Object.prototype.bind_property_full()`. Evan fixed `GLib.log_set_writer_func()`. Other various bug fixes from Evan and Florian as well as Chun-wei Fan and myself.

### Connections [↗](https://gitlab.gnome.org/GNOME/connections/)

A remote desktop client.

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> Felipe Borges and Jakub Steiner [updated the Connections onboarding](https://gitlab.gnome.org/GNOME/connections/-/merge_requests/88), to have more steps and clearer illustrations.
> ![](eca3749309f70fccb4e94bd5b0e957b22f3babe6.png)

### GNOME OS [↗](https://os.gnome.org)

The closest to upstream you will ever get.

[jjardon](https://matrix.to/#/@jjardon:matrix.org) announces

> The nightly OS images we build and publish at os.gnome.org now have automated testing done by OpenQA. This will help us ensure they stay working and is a great tool to detect regressions in our platform at integration time. More details in the announcement in discourse: https://discourse.gnome.org/t/introducing-openqa-gnome-org/7270 This work was kindly sponsored by Codethink, you can read the blog post we have done about this work here: https://www.codethink.co.uk/articles/2021/gnome-with-openqa/
> ![](FqQTlAHupSoBWywJVgYGlqQd.png)

# Circle Apps and Libraries

### Health [↗](https://gitlab.gnome.org/Cogitri/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) announces

> GSoC intern Visvesh [added support in Health](https://gitlab.gnome.org/Cogitri/Health/-/merge_requests/118) for sending the user notifications to remind them that they haven't met their daily step goal yet.

### Cozy [↗](https://github.com/geigi/cozy/)

An audiobook reader and manager.

[geigi](https://matrix.to/#/@geigi7:matrix.org) announces

> Cozy v1.1.0 featuring an UI redesign has been released! It's also the first step to fully support mobile by adopting libhandy in many places.
> ![](IzLYNDsLpYgjfnZVFCGZxEUD.png)

### Metadata Cleaner [↗](https://gitlab.com/rmnvgr/metadata-cleaner)

View and clean metadata in files.

[Romain](https://matrix.to/#/@romainvigier:matrix.org) reports

> [Metadata Cleaner](https://gitlab.com/rmnvgr/metadata-cleaner/) has been updated and is now available in 14 languages! [Help translate it](https://hosted.weblate.org/engage/metadata-cleaner/) to your language for the next release, which will feature a brand new UI built with GTK4 and libadwaita.

### Decoder [↗](https://gitlab.gnome.org/World/decoder/)

Scan and Generate QR Codes.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> Decoder 0.2.0 has just been released. In this update we feature one of the first real world uses of the Freedesktop camera portal thanks to the ASHPD library and PipeWire. You can read more about it at [my blog](https://blogs.gnome.org/msandova/2021/08/07/decoder-0-2-0-released). You can get Decoder at [Flathub](https://flathub.org/apps/details/com.belmoussaoui.Decoder).
> ![](625ad37001e13bb9c37d4c9d0f2ad95778429cc6.png)

# Third Party Projects

### Lorem [↗](https://gitlab.gnome.org/World/design/lorem)

Placeholders a few clicks away.

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) reports

> Introducing Lorem, a new app to generate tasteful placeholder text for you projects.
> 
> You can get Lorem at [Flathub](https://flathub.org/apps/details/org.gnome.design.Lorem).
> ![](91f22b3d65a55e324af707c7a484faa74cd39b08.png)

### Metronome [↗](https://gitlab.gnome.org/aplazas/metronome)

Practice music with a regular tempo.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) announces

> I released the first version of Metronome, [get it on Flathub](https://flathub.org/apps/details/com.adrienplazas.Metronome).
> ![](16a49e5391c34a8c5944577c9b9a93d1744774a6.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) reports

> In our fractal-next branch, Julian [tweaked the style of the ugly “this is an early development version, here be dragons” warning](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/800), while GSoC intern Kai did a couple maintenance tasks: he [updated some documentation](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/809) and [fixed clippy warnings](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/812).
> 
> The stable branch also saw some activity, with new contributor 🎉 Amanda fixing _two bugs_ ([#392](https://gitlab.gnome.org/GNOME/fractal/-/issues/392), [#803](https://gitlab.gnome.org/GNOME/fractal/-/issues/803)) with a single merge request, [sorting out our handling of <p> tag trimming](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/813).

# Documentation

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) reports

> I have added a new [chapter](https://gtk-rs.org/gtk4-rs/stable/latest/book/interface_builder.html) about the interface builder to "GUI development with Rust and GTK 4".
> ![](6acc208fa98fb57ae882db9623175bb158a3057a.png)

# Miscellaneous

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> The ["This Week in GNOME"](https://thisweek.gnome.org/) website got few improvements. Rafael Mardojai CM added [heading anchors](https://gitlab.gnome.org/World/twig/-/merge_requests/2) to make it easier to link to specific news and added [a shiny symbolic icon](https://gitlab.gnome.org/World/twig/-/merge_requests/6) for the project links.
> 
> [Hebbot](https://github.com/haecker-felix/hebbot) (the Matrix bot which generates the TWIG blog posts) got upgraded to v2.0. It has learned how to handle images / videos, and can now insert them directly into the rendered markdown. Creating new posts has been significantly simplified and now makes even less work for the author. For example, Hebbot now automatically generates a command to quickly download all images/videos at once. More information can be found in the [release notes](https://github.com/haecker-felix/hebbot/releases/tag/v2.0).
> ![](ZNIHgDsqChRnlgxklaRgGiQP.png)

[C Wunder](https://matrix.to/#/@ovflowd:gnome.org) says

> We reached 60K members @ our Subreddit (r/GNOME). 🎉

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

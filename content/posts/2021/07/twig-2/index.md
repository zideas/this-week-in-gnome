---
title: "#2 Power-Up!"
author: Felix
date: 2021-07-23
tags: ["gnome-podcasts", "gnome-shell", "gnome-builder", "mutter", "tracker", "gnome-calendar", "tootle", "blanket"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 16 to July 23.<!--more-->

# Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> The Power panel of the Settings app received various improvements on power profiles, like the ability to automatically switch to "Power Saver" mode when low on battery, indications of which apps requested a different power profile, and even lap detection on computer models that support it.
> ![](gnome-control-center1.png)
> ![](gnome-control-center2.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> The new screenshot UI got a few updates:
> * it's now possible to drag the entire selection rectangle around (rather than just its corners)
> * screen selection now lets you to select the monitor to capture
> * window selection now works fine with multiple monitors and has a dark shade background to make windows more visible
> 
> {{< video src="shell1" >}}

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> [GNOME Shell now uses unaccelerated deltas for its gestures](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1763), like the 3-finger gestures to enter and exit the Overview, and switch workspaces. This unifies the speed of the gestures, making they behave more uniformly regardless of how fast you move your fingers on the touch device.

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter)

A Wayland display server and X11 window manager and compositor library.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> A [major reorganization of Mutter's backend landed](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1862), deleting almost 2,000 lines of code and simplifying the overall structure of the code.

### Tracker [↗](https://gnome.pages.gitlab.gnome.org/tracker/)

A filesystem indexer, metadata storage system and search tool. 

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> Tracker database engine is now available on Mac OS X via [Homebrew](https://github.com/Homebrew/homebrew-core/pull/80281), thanks to Daniele Nicolodi and Cogitri

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[gwagner](https://matrix.to/#/@gwagner:gnome.org) reports

> Builder supports now Gtk4 templates. Currently only C templates are available. Contributions welcome!
> ![](gnome-builder1.png)

### Calendar [↗](https://wiki.gnome.org/Apps/Calendar)

A simple calendar application.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> GNOME Calendar received an event preview popover, providing quick access to event information and meetings URLs
> ![](gnome-calendar1.png)

# Circle Apps and Libraries
### Tootle [↗](https://github.com/bleakgrey/tootle)

Lightning fast client for Mastodon.

[Bleak Grey](https://matrix.to/#/@bleak.grey:matrix.org) announces

> [Tootle](https://github.com/bleakgrey/tootle), the native Mastodon client for GNOME, got ported to GTK4 and Adwaita. The new alpha release has seen a major interface redesign, better cache handling, and overall responsiveness boost.
> 
> While not entirely supported yet, it also supports logging in to Pleroma instances.
> ![](tootle1.png)

### Blanket [↗](https://github.com/rafaelmardojai/blanket)

Improve focus and increase your productivity by listening to different sounds.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Blanket now will let you save different presets, allowing you to manage your favorite sound combos easily.
> ![](blanket1.png)

### Podcasts [↗](https://gitlab.gnome.org/podcasts)

Podcast app for GNOME.

[nee](https://matrix.to/#/@nee:tchncs.de) says

> [Podcasts](https://wiki.gnome.org/Apps/Podcasts) now [supports](https://gitlab.gnome.org/World/podcasts/-/merge_requests/190) importing soundcloud playlist podcasts.
> {{< video src="gnome-podcasts1" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

